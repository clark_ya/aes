#ifndef __AES_H
#define __AES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define AES128_NK  16 /**< AES128 input key length. */
#define AES192_NK  24 /**< AES192 input key length. */
#define AES256_NK  32 /**< AES256 input key length. */
#define AES128_NB 176 /**< AES128 expanded key length. */
#define AES192_NB 208 /**< AES192 expanded key length. */
#define AES256_NB 240 /**< AES256 expanded key length. */
#define AES_BLOCK  16 /**< AES block length. */

typedef uint8_t* aes_byteptr;

/**
 *	Expands a 16 byte key to 176 bytes.
 *	This function must be called before aes128_encrypt/decrypt.
 *	@key Key buffer of AES128_NB length. Put the input key in the first 
 *	     AES128_NK bytes. The expansion function fills in the rest.
 */
void aes128_expand(aes_byteptr key);

/**
 *	Expands a 24 byte key to 208 bytes.
 *	This function must be called before aes192_encrypt/decrypt.
 *	@key Key buffer of AES192_NB length. Put the input key in the first 
 *	     AES192_NK bytes. The expansion function fills in the rest.
 */
void aes192_expand(aes_byteptr key);

/**
 *	Expands a 32 byte key to 240 bytes.
 *	This function must be called before aes256_encrypt/decrypt.
 *	@key Key buffer of AES256_NB length. Put the input key in the first 
 *	     AES256_NK bytes. The expansion function fills in the rest.
 */
void aes256_expand(aes_byteptr key);

/**
 *	Encrypts a plaintext block with an expanded key.
 *	@key       Pointer to the expanded AES128 key.
 *	@block     The plaintext block of AES_BLOCK length.
 *	@block_out The ciphertext output block.
 */
void aes128_encrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

/**
 *	Encrypts a plaintext block with an expanded key.
 *	@key       Pointer to the expanded AES192 key.
 *	@block     The plaintext block of AES_BLOCK length.
 *	@block_out The ciphertext output block.
 */
void aes192_encrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

/**
 *	Encrypt a plaintext block with an expanded key.
 *	@key       Pointer to the expanded AES256 key.
 *	@block     The plaintext block of AES_BLOCK length.
 *	@block_out The ciphertext output block.
 */
void aes256_encrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

/**
 *	Decrypt a ciphertext block with an expanded key using AES128.
 *	@key       Pointer to the expanded AES128 key.
 *	@block     The ciphertext block of AES_BLOCK length.
 *	@block_out The plaintext output block.
 */
void aes128_decrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

/**
 *	Decrypt a ciphertext block with an expanded key using AES192.
 *	@key       Pointer to the expanded AES192 key.
 *	@block     The ciphertext block of AES_BLOCK length.
 *	@block_out The plaintext output block.
 */
void aes192_decrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

/**
 *	Decrypt a ciphertext block with an expanded key using AES256.
 *	@key       Pointer to the expanded AES256 key.
 *	@block     The ciphertext block of AES_BLOCK length.
 *	@block_out The plaintext output block.
 */
void aes256_decrypt(const aes_byteptr key, const aes_byteptr block, aes_byteptr block_out);

#ifdef __cplusplus
}
#endif

#endif
