# AES Library in C(99)

Yes, I know. Never implement your own cryptography. This was just an excercise to see if I could translate the AES specification into code - and I succeeded! I even optimised the implementation as much as I could and there is a program to verify that implementation is correct.

The actual implementation is very simple. It only provides three AES primitives: Key expansion, encrypt block, and decrypt block for each of the AES block sizes (128, 192, 256). Nothing more, nothing less. You can implement your favorite cipher mode yourself using these primitives.